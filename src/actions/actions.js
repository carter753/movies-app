import clapperboard from '../assets/img/clapperboard-film.jpg';
import { 
  clearingArrayRepetitions,
  splitNumberByComma,
  fitCountries,
  fitRuntime,
  sortArray,
  fitGenres
} from '../utils/helpers';
import {
  API_EXACT_SEARCH_START,
  API_BACKDROP_IMG,
  API_POSTER_IMG, 
  API_LANGUAGE,
  API_CREDITS,
  API_KEY
} from '../constants/api';

export const responseHasErrored = bool => ({
  type: 'RESPONSE_HAS_ERRORED',
  hasErrored: bool
});

export const responseIsLoading = bool => ({
  type: 'RESPONSE_IS_LOADING',
  isLoading: bool
});

export const responseSuccess = items => ({
  type: 'RESPONSE_SUCCESS',
  items
});

export const repeatedResponseSuccess = item => ({
  type: 'REPEATED_RESPONSE_SUCCESS',
  item
});

export const typingInInput = value => ({
  type: 'TYPING_IN_INPUT',
  value
});

export const selectMovie = movie => ({
  type: 'SELECT_MOVIE',
  movie
})

export const addMovieToFavourites = movie => ({
  type: 'ADD_MOVIE_TO_FAVOURITES',
  movie
})

export const addMovieToNearestView = movie => ({
  type: 'ADD_MOVIE_TO_NEAREST_VIEW',
  movie
})

export const addMovieToViewed = movie => ({
  type: 'ADD_MOVIE_TO_VIEWED',
  movie
})

export const initialFetchData = url => {
  return dispatch => {
    dispatch(responseIsLoading(true));

    fetch(url) 
      .then(response => {
        if (!response.ok) {
          throw Error(response.statusText)
        }

        dispatch(responseIsLoading(false));

        return response;
      })
      .then(response => response.json())
      .then(data => dispatch(responseSuccess(data.results)))
      .catch(() => dispatch(responseHasErrored(true)));
  };
}

export const exactFetchData =  event => {
  return dispatch => {
    dispatch(responseIsLoading(true));

    const movieId = event.target.id;

    fetch(API_EXACT_SEARCH_START + movieId + API_KEY + API_LANGUAGE)
      .then(response => {
        if (!response.ok) {
          throw Error(response.statusText)
        }
        dispatch(responseIsLoading(false));
        return response;
      })
      .then(response => response.json())
      .then(data => {  
        let countries = fitCountries(data.production_countries);
        let revenue = splitNumberByComma(data.revenue);
        let budget = splitNumberByComma(data.budget);
        let runtime = fitRuntime(data.runtime);
        let genres = fitGenres(data.genres);

        const dataMovie = {
          background: data.backdrop_path ? API_BACKDROP_IMG + data.backdrop_path : clapperboard,
          image: data.poster_path ? API_POSTER_IMG + data.poster_path : clapperboard,
          year: data.release_date ? data.release_date.slice(0, 4) : null,
          runtime: data.runtime ? runtime : null,
          revenue: data.revenue ? revenue : null, 
          budget: data.budget ? budget : null,
          vote_average: data.vote_average,
          overview: data.overview,
          imdbId: data.imdb_id,
          country: countries,
          title: data.title,
          genre: genres,
          id: data.id,
        } 

        fetch(API_EXACT_SEARCH_START + movieId + API_CREDITS + API_KEY)
          .then(response => response.json())
          .then(credit => {
            let crewShortRaw = credit.crew.filter(item => item.job === 'Writer' || item.job === 'Director' || item.job === 'Producer')

            const crewShort = clearingArrayRepetitions(crewShortRaw, 'name', 'job');
            const sortedCrew = sortArray(credit.crew, 'department');
      
            dataMovie.cast = credit.cast;
            dataMovie.crew = sortedCrew;
            dataMovie.crew_short = crewShort;
            dataMovie.crew_qty = credit.crew.length;

            dispatch(repeatedResponseSuccess(dataMovie));
          })
      })
      .catch(() => dispatch(responseHasErrored(true)));
  }
}