import React from 'react';
import '../styles/App.css';

const Button = ({ text, method }) => (
  <button className='button' onClick={method}>{text}</button>
)

export default Button