import React from 'react';

const Input = ({ handleInput }) => (
  <form>
    <input type="text" onChange={handleInput}/>
  </form>
)


export default Input;