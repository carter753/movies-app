import React from 'react';
import spinner from '../assets/img/loading_spinner.png';

const Loading = () => (
  <div>
    <img src={spinner} alt='' />
  </div>
)

export default Loading;