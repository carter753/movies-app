import React from 'react';
import StorageFavouritesMovies from './StorageFavouritesMovies';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';

const ManagementFavouritesMovies = () => (
  <DndProvider backend={HTML5Backend}>
    < StorageFavouritesMovies />
  </DndProvider>
)

export default ManagementFavouritesMovies;