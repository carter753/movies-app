import React from 'react';
import shortid from 'shortid';
import { connect } from 'react-redux';
import profile from '../assets/img/profile.png';
import {API_PORTRAIT_IMG} from '../constants/api';

const MovieCompleteCredit = ({ item }) => (    
  <div className="complete-actors-list">
    <div className="complete-actors-list__item">
      <div className="complete-actors-list__header">
        <h3 className="complete-actors-list__title">Cast</h3>
        <span className="complete-actors-list__qty">{item.cast.length}</span>
      </div>
      {item.cast.map(item => (
        <div className="credits__card  credits__card--complete" key={shortid.generate()}>
          <div className="credits__poster">
            <img className="credits__img  credits__img--complete" src={item.profile_path ? API_PORTRAIT_IMG + item.profile_path : profile} alt=""/>
          </div>
          <div className="credits__content">
            <p className="credit__name">{item.name}</p>
            <p className="credit__character">{item.character}</p>
          </div>
        </div>
      ))}
    </div>
    <div className="complete-actors-list__item">
      <div className="complete-actors-list__header">
        <h3 className="complete-actors-list__title">Crew</h3>
        <span className="complete-actors-list__qty">{item.crew_qty}</span>
      </div>
      {item.crew.map(item => {
        if (typeof item === 'string') {
          return (
            <h4 className="department__name" key={shortid.generate()}>{item}</h4>
          )
        } else {
          return (
            <div className="credits__card  credits__card--complete" key={shortid.generate()}>
              <div className="credits__poster credits__poster--complete">
                <img className="credits__img  credits__img--complete" src={item.profile_path ? API_PORTRAIT_IMG + item.profile_path : profile} alt=""/>
              </div>
              <div className="credits__content">
                <p className="credit__name">{item.name}</p>
                <p className="credit__character">{item.job}</p>
              </div>
            </div>
          )
        }})}
    </div>
  </div>
)

const mapStateToProps = state => ({
  item: state.item
});

export default connect(mapStateToProps)(MovieCompleteCredit);