import React, { useState } from 'react';
import { useDrop } from 'react-dnd';
import { ItemTypes } from '../types/ItemTypes';
import update from 'immutability-helper';
import CategoryMovie from '../containers/CategoryMovie';
import FavouriteMovie from '../containers/FavouriteMovie';

const StorageFavouritesMovies = () => {
  const [values, setValues] = useState({
    widthDragElement: 0,
    heightDragElement: 0,
    topDragElement: 0,
    leftDragElement: 0,
  })

  const [, drop] = useDrop({
    accept: ItemTypes.BOX,
    drop(item, monitor) {
      const delta = monitor.getDifferenceFromInitialOffset();
      if (delta !== null) {
        const left = Math.round(values.leftDragElement + delta.x);
        const top = Math.round(values.topDragElement + delta.y);
        moveBox(left, top);
      } 
    }
  })

  const moveBox = (left, top) => {
    setValues(
      update(values, {
        $merge: { left, top } 
      })
    )
  }
  
  return (
    <div ref={drop}>
      <FavouriteMovie 
        values={values}
        setValues={setValues}
        hideSourceOnDrag={true}
      />
      <div className='categories'>
        <CategoryMovie values={values} setValues={setValues} />
        <CategoryMovie values={values} setValues={setValues} />
        <CategoryMovie values={values} setValues={setValues} />
      </div>
    </div>
  )
}

export default StorageFavouritesMovies;