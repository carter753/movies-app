export const MANAGEMENT_FAVOURITES_MOVIES='/management-favourites-movies';
export const COMPLETE_CREDIT_LIST = '/complete-credit-list';
export const VIEW_DETAILS_MOVIE = '/view-details-movie';
export const SEARCH_MOVIE = '/search-movie';
export const DEFAULT = '/';