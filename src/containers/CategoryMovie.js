import React, { useRef } from 'react';
import { useDrop } from 'react-dnd';
import { connect } from 'react-redux';
import { ItemTypes } from '../types/ItemTypes';
import update from 'immutability-helper';
import { 
  addMovieToFavourites, 
  addMovieToNearestView, 
  addMovieToViewed 
} from '../actions/actions';

const CategoryMovie = ({ 
  addMovieToFavouritesMovies,
  addMovieToViewedMovies,
  addMovieToNearestView,
  nearestViewMovies,
  favouritesMovies,
  selectedMovie,
  viewedMovies,
  setValues,
  values, 
}) => {
  const wrapper = useRef();

  const [{ canDrop, isOver }, drop] = useDrop({
    accept: ItemTypes.BOX,
    drop: (item, monitor) => {
      const heightDropElement = wrapper.current.getBoundingClientRect().height;
      const widthDropElement = wrapper.current.getBoundingClientRect().width;
      const leftDropElement = wrapper.current.getBoundingClientRect().left;
      const topDropElement = wrapper.current.getBoundingClientRect().top;
      
      const heightDragElement = values.heightDragElement;
      const widthDragElement = values.widthDragElement;
      
      const pointY = (heightDropElement - heightDragElement) / 2 + topDropElement;
      const pointX = (widthDropElement - widthDragElement) / 2 + leftDropElement;

      moveBox(pointY, pointX);

      switch(monitor.targetId) {
        case 'T1':
          addMovieToFavouritesMovies(selectedMovie);
          break;
        case 'T2':
          addMovieToNearestView(selectedMovie);
          break;
        case 'T3':
          addMovieToViewedMovies(selectedMovie);
          break;
        default:
          return null;
      }
    },
    collect: monitor => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }), 
  })

  const toggleBoxShadow = (isActive, canDrop) => {
    if (isActive) {
      return '0 0 15px rgba(251, 255, 5, .8)'
    } else {
      return 'none'
    }
  }

  const isActive = canDrop && isOver
  const boxShadow = toggleBoxShadow(isActive, canDrop)

  const moveBox = (topDragElement, leftDragElement) => { 
    setValues(
      update(values, {
        $merge: { topDragElement, leftDragElement } 
      })
    )
  }

  return (
    <div ref={wrapper} className='category-movie' style={{boxShadow}}>
      <div ref={drop} className='category-movie__drop' />
    </div>

  )
}

const mapStateToProps = state => ({
  nearestViewMovies: state.nearestViewMovies, 
  favouritesMovies: state.favouritesMovies, 
  selectedMovie: state.selectedMovie,
  viewedMovies: state.viewedMovies,
})

const mapDispatchToProps = dispatch => ({
  addMovieToFavouritesMovies: movie => dispatch(addMovieToFavourites(movie)),
  addMovieToNearestView: movie => dispatch(addMovieToNearestView(movie)),
  addMovieToViewedMovies: movie => dispatch(addMovieToViewed(movie))
})

export default connect(mapStateToProps, mapDispatchToProps)(CategoryMovie);