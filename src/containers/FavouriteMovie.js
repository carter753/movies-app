import React, { useRef } from 'react';
import '../styles/App.css';
import { useDrag } from 'react-dnd';
import { connect } from 'react-redux';
import { ItemTypes } from '../types/ItemTypes';

const FavouriteMovie = ({ 
  hideSourceOnDrag,
  selectedMovie,
  setValues, 
  values
}) => {
  const wrapper = useRef();

  const top = values.topDragElement;
  const left = values.leftDragElement;

  const [{ isDragging }, drag] = useDrag({
    item: {type: ItemTypes.BOX},
    collect: (monitor) => ({
      isDragging: monitor.isDragging()
    }),
    begin: () => {
      const heightDragElement = wrapper.current.getBoundingClientRect().height;
      const widthDragElement = wrapper.current.getBoundingClientRect().width;
      const leftDragElement = wrapper.current.getBoundingClientRect().left;
      const topDragElement = wrapper.current.getBoundingClientRect().top;
      
      setValues({ 
        widthDragElement,
        heightDragElement,
        topDragElement,
        leftDragElement
       })
    } 
  })

  if (isDragging && hideSourceOnDrag) {
    return <div ref={drag} className='favourite-movie__dragged'/>
  }

  return (
    <div ref={wrapper} className='favourite-movie' style={{ top, left }}>
      <div ref={drag} className='favourite-movie__dragged'>
        <img className='favourite-movie__img' src={selectedMovie.image} alt=''/>
        <div className='favourite-movie__text'>
          <p className='favourite-movie__title'>{selectedMovie.title}</p>
          <p className='favourite-movie__year'>{selectedMovie.year}</p>
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = state => ({
  selectedMovie: state.selectedMovie,
});

export default connect(mapStateToProps)(FavouriteMovie);