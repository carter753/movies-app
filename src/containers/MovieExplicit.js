import React from 'react';
import '../styles/App.css';
import Button from '../components/Button';
import { connect } from 'react-redux';
import profile from '../assets/img/profile.png';
import { useHistory } from 'react-router-dom';
import { selectMovie } from '../actions/actions';
import { API_PORTRAIT_IMG } from '../constants/api';
import clapperboard from '../assets/img/clapperboard-film.jpg';
import { 
  MANAGEMENT_FAVOURITES_MOVIES, 
  COMPLETE_CREDIT_LIST 
} from '../constants/routes';

const MovieExplicit= ({ item, selectedMovie, addMovie }) => {
  let history = useHistory();

  const viewCompleteCreditList = () => {
    history.push(COMPLETE_CREDIT_LIST);
  }

  const manageFavouritesMovies = () => {
    history.push(MANAGEMENT_FAVOURITES_MOVIES);
  }

  const addMovieToFavorites = () => {
    addMovie(item);
  }

  return (
    <div>
      <div className="explicit">
        <div className="explicit__container">
          <div className="explicit__inner">
            <div className="explicit__poster">
              <img className="explicit__img" src={item.image} alt={clapperboard} />
            </div>
            <div className="explicit__content">
              <div className="explicit__header">
                {item.title && <span className="explicit__title">{item.title}</span>}
                {item.year && <span className="explicit__year">({item.year})</span>}
              </div>
              <div className="explicit__subheader">
                {item.genre && <span className="explicit__genre">{item.genre}</span>}
                <div className='explicit__split'></div>
                {item.runtime && <span className="explicit__runtime">{item.runtime}</span>}
              </div>
              {item.overview && <p className="explicit__overview">{item.overview}</p>}
              {item.country && <p className="explicit__country">
                <span className="explicit__text explicit__text--bold">
                  {(item.country.includes(',')) ? 'Countries: ' : 'Country: '}
                </span>
                <span className="explicit__text">{item.country}</span>
              </p>}
              {item.revenue && <p className="explicit__revenue">
                <span className="explicit__text explicit__text--bold">Revenue: </span>
                <span className="explicit__text">{item.revenue}</span>
              </p>}
              {item.budget && <p className="explicit__budget">
                <span className="explicit__text explicit__text--bold">Budget: </span>
                <span className="explicit__text">{item.budget}</span>
              </p>}
              {item.imdbId && <p className="explicit__imdb">
                <span className="explicit__text explicit__text--bold">IMDb: </span>
                <span className="explicit__text">{item.imdbId}</span>
              </p>}
              {item.crew_short &&  <div className="explicit__crew">
                {item.crew_short.map(item => (
                  <div key={item.credit_id} className="explicit__filmmaker">
                    <p className="explicit__name">{item.name}</p>
                    <p className="explicit__job">{item.job}</p>
                  </div>
                ))}
              </div>}
              <Button text='Add Movie' method={addMovieToFavorites}/>
              <Button text='Manage Favourites Movies' method={manageFavouritesMovies} />
            </div>
          </div>
        </div>
      </div>
      <div className="credits">
        <div className="credits__container">
          <div className="credits__inner">
            {item.cast.map((item, index) => {
              if (index <= 5) {
                return (
                  <div className="credits__card" key={item.id}>
                    <div className="credits__poster">
                      <img className="credits__img" src={item.profile_path ? API_PORTRAIT_IMG + item.profile_path : profile} alt=""/>
                    </div>
                    <div className="credits__content">
                      <p className="credit__name">{item.name}</p>
                      <p className="credit__character">{item.character}</p>
                    </div>
                  </div>
                ) 
              } 
            })}
            <button onClick={viewCompleteCreditList}>See more</button>
          </div>
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = state => ({
  selectedMovie: state.selectedMovie,
  item: state.item,
});

const mapDispatchToProps = dispatch => ({
  addMovie: movie => dispatch(selectMovie(movie))
});

export default connect(mapStateToProps, mapDispatchToProps)(MovieExplicit);
