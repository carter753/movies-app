import React from 'react';
import '../styles/App.css';
import Button from '../components/Button';
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { selectMovie } from '../actions/actions';
import clapperboard from '../assets/img/clapperboard-film.jpg';
import { 
  MANAGEMENT_FAVOURITES_MOVIES, 
  VIEW_DETAILS_MOVIE 
} from '../constants/routes';

const MoviePreview = ({ item, selectedMovie, addMovie }) => {  
  let history = useHistory();

  const viewDetails = () => {
    history.push(VIEW_DETAILS_MOVIE);
  }

  const manageFavouritesMovies = () => {
    history.push(MANAGEMENT_FAVOURITES_MOVIES);
  }

  const addMovieToFavorites = () => {
    addMovie(item);
  }

  return (
    <div className="preview">
      <div className="preview__container">
        <div className="preview__inner">
          <div className="preview__poster">
            <img className="preview__img  preview__img--small" src={item.image} alt={clapperboard} />
          </div>
          <div className="preview__content">
            <div className="preview__header">
              {item.title && <span className="preview__title">{item.title}</span>}
              {item.year && <span className="preview__year">({item.year})</span>}
            </div>
            {item.genre && <p className="preview__genre">{item.genre}</p>}
            {item.overview && <p className="preview__overview">{item.overview}</p>}
            <Button text='See more' method={viewDetails} />
            <Button text='Add Movie' method={addMovieToFavorites} />
            <Button text='Manage Favourites Movies' method={manageFavouritesMovies} />
          </div>
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = state => ({
  item: state.item,
  selectedMovie: state.selectedMovie,
});

const mapDispatchToProps = dispatch => ({
  addMovie: movie => dispatch(selectMovie(movie))
});

export default connect(mapStateToProps, mapDispatchToProps)(MoviePreview);