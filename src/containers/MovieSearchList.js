import React from 'react';
import '../styles/App.css';
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { API_POSTER_IMG } from '../constants/api';
import { SEARCH_MOVIE } from '../constants/routes';
import { exactFetchData } from '../actions/actions';
import clapperboard from '../assets/img/clapperboard-film.jpg';

const MovieSearchList = ({
  getExactMovie, 
  items,
  value,
  item
}) => {

  let history = useHistory();

  const wishMovie = id => {
    getExactMovie(id);
    history.push(SEARCH_MOVIE);
  }

  return (
  <div className="results">
      {items.map(item => (
        <div key={item.id} className="result" id={item.id}>
          <div className="result__poster">
            <img 
              className="result__img" 
              id={item.id} 
              src={item.poster_path ? API_POSTER_IMG + item.poster_path : clapperboard} 
              alt={clapperboard} 
              onClick={wishMovie}
            />
          </div>
          <div className="result__content">
            <h4 className="result__title">{item.title}</h4>
            <p className="result__year">
              {item.release_date ? item.release_date.slice(0, 4) : null}
            </p>
          </div>
        </div>
        ))}  
    </div>
  )
}

const mapStateToProps = state => ({
  item: state.item,
  items: state.items,
  value: state.valueSearchInput,
});

const mapDispatchToProps = dispatch => ({
  getExactMovie: id => dispatch(exactFetchData(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(MovieSearchList);
