import React from 'react';
import { connect } from 'react-redux';
import Error from '../components/Error';
import Input from '../components/Input';
import Button from '../components/Button';
import Loading from '../components/Loading';
import { getComponentOrNull } from '../utils/helpers';
import MovieNotFound from '../components/MovieNotFound';
import MovieSearchList from '../containers/MovieSearchList';
import { 
  initialFetchData, 
  exactFetchData, 
  typingInInput
} from '../actions/actions';
import {
  API_INITIAL_SEARCH_START,
  API_LANGUAGE,
  API_QUERY, 
  API_KEY
} from '../constants/api';
import { MANAGEMENT_FAVOURITES_MOVIES } from '../constants/routes';

const MovieStartSearch = ({ 
  getExactMovie, 
  getValueInput,
  hasErrored,
  isLoading, 
  fetchData, 
  value, 
  items, 
  item
}) => {
  const handleInput = event => {
    let value = event.target.value;
    if (value) {
      fetchData(API_INITIAL_SEARCH_START + API_KEY + API_LANGUAGE + API_QUERY + value);
    }
    getValueInput(value);
  }

  const conditionShowingMoviesList = (
    isLoading === false && 
    hasErrored === false && 
    items.length > 0 &&
    value.length > 0
  );

  const conditionMovieNotFound = (
    isLoading === false && 
    hasErrored === false &&
    items.length === 0 &&
    value.length > 0
  )

  const getComponentMoviesList = getComponentOrNull(<MovieSearchList />, conditionShowingMoviesList);
  const getComponentMovieNotFound = getComponentOrNull(<MovieNotFound />, conditionMovieNotFound);
  const getComponentIsLoading = getComponentOrNull(<Loading />, isLoading);
  const getComponentHasErrored = getComponentOrNull(<Error />, hasErrored);
    
  return (
    <div>
      <Input handleInput={handleInput}/>
      {getComponentIsLoading} 
      {getComponentHasErrored}
      {getComponentMoviesList}
      {getComponentMovieNotFound}
    </div>
  )
}

const mapStateToProps = state => ({
  item: state.item,
  items: state.items,
  value: state.valueSearchInput,
  selectedMovie: state.selectedMovie,
  isLoading: state.responseIsLoading,
  hasErrored: state.responseHasErrored
});

const mapDispatchToProps = dispatch => ({
  fetchData: url => dispatch(initialFetchData(url)),
  getValueInput: value => dispatch(typingInInput(value)),
  getExactMovie: id => dispatch(exactFetchData(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(MovieStartSearch)
