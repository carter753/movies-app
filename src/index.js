import React from 'react';
import { render } from 'react-dom';
import { configureStore } from './store/configureStore';
import Router from './routes/Router';
import './styles/index.css';

const store = configureStore();

render (
  <Router store={store} />,
  document.getElementById('root')
)
