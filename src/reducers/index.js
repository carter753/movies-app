import { combineReducers } from 'redux';
import { 
  responseHasErrored, 
  responseIsLoading, 
  nearestViewMovies,
  valueSearchInput,
  favouritesMovies,
  selectedMovie,
  viewedMovies,
  items,
  item 
} from './reducers';

export default combineReducers({
  responseHasErrored, 
  responseIsLoading, 
  nearestViewMovies,
  valueSearchInput,
  favouritesMovies,
  selectedMovie,
  viewedMovies,
  items,
  item 
});