export const responseHasErrored = (state = false, action) => {
  switch(action.type) {
    case 'RESPONSE_HAS_ERRORED':
      return action.hasErrored;
    default:
      return state;
  }
};

export const responseIsLoading = (state = false, action) => {
  switch (action.type) {
    case 'RESPONSE_IS_LOADING':
      return action.isLoading;
    default:
      return state;
  }
}

export const items = (state = [], action) => {
  switch(action.type) {
    case 'RESPONSE_SUCCESS':
      return action.items;
    default:
      return state;
  }
}

export const item = (state = {}, action) => {
  switch(action.type) {
    case 'REPEATED_RESPONSE_SUCCESS':
      return action.item
    default:
      return state;
  }
}

export const valueSearchInput = (state = '', action) => {
  switch(action.type) {
    case 'TYPING_IN_INPUT':
      return action.value
    default:
      return state;
  }
}

export const selectedMovie = (state = {}, action) => {
  switch(action.type) {
    case 'SELECT_MOVIE':
      return action.movie;
    default:
      return state;
  }
}

export const favouritesMovies = (state = [], action) => {
  switch(action.type) {
    case 'ADD_MOVIE_TO_FAVOURITES':
      let newState = state;
      newState.push(action.movie);
      return newState;
    default:
      return state;
  }
}

export const nearestViewMovies = (state = [], action) => {
  switch(action.type) {
    case 'ADD_MOVIE_TO_NEAREST_VIEW':
      let newState = state;
      newState.push(action.movie);
      return newState;
    default:
      return state;
  }
}

export const viewedMovies = (state = [], action) => {
  switch(action.type) {
    case 'ADD_MOVIE_TO_VIEWED':
      let newState = state;
      newState.push(action.movie);
      return newState;
    default:
      return state;
  }
}