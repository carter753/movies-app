import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import ManagementFavouritesMovies from '../components/ManagementFavouritesMovies';
import MovieCompleteCredit from '../components/MovieCompleteCredit';
import MovieStartSearch from '../containers/MovieStartSearch';
import MovieExplicit from '../containers/MovieExplicit';
import MoviePreview from '../containers/MoviePreview';

import {
  MANAGEMENT_FAVOURITES_MOVIES,
  COMPLETE_CREDIT_LIST,
  VIEW_DETAILS_MOVIE,
  SEARCH_MOVIE,
  DEFAULT
} from '../constants/routes';

const Root = ({ store }) => (
  <Provider store={store}>
    <Router>
      <Route path={MANAGEMENT_FAVOURITES_MOVIES} component={ManagementFavouritesMovies} /> 
      <Route path={COMPLETE_CREDIT_LIST} component={MovieCompleteCredit} />
      <Route path={VIEW_DETAILS_MOVIE} component={MovieExplicit} />
      <Route exact path={DEFAULT} component={MovieStartSearch} />
      <Route path={SEARCH_MOVIE} component={MoviePreview} />
    </Router>
  </Provider>
)

export default Root;