export const getComponentOrNull = (component: Component, condition: boolean) => 
  condition ? component : null;

export const clearingArrayRepetitions = (array, savedElements, deletedElements) => {
  let arrayRepeatingElements = [],
      counterDifferentElement = 0,
      counterEqualElements = 0,
      arrayStartPart = [],
      arrayEndPart = [],
      arrayFinish = [];

  for (let i = 0; i < array.length; i++) {
    for (let k = 0; k < array.length; k++) {
      if (array[i][savedElements] === array[k][savedElements]) {
        counterEqualElements++;
      } else {
        counterDifferentElement++;
      }
    }

    if (counterDifferentElement === array.length - 1) {
      arrayStartPart.push(array[i]);
    } else {
      arrayEndPart.push(array[i]);
      arrayRepeatingElements.push(array[i][deletedElements]);
      arrayEndPart[0][deletedElements] = arrayRepeatingElements.join(', ');
      arrayEndPart.splice(1, arrayEndPart.length - 1);
    }
      counterEqualElements = 0;
      counterDifferentElement = 0;
  }
  arrayFinish = arrayStartPart.concat(arrayEndPart);
  return arrayFinish;
}

export const sortArray = (array, criterionSort) => {
  let criterionSortArrayRaw = array.map(item => item[criterionSort]);
  let criterionSortArray = criterionSortArrayRaw.filter((item, index) => {
    return criterionSortArrayRaw.indexOf(item) === index
    });
  let sortedArray = [];

  for (let i = 0; i < criterionSortArray.length; i++) {
    for (let k = 0; k < array.length; k++) {
      if (criterionSortArray[i] === array[k][criterionSort]) {
        if (sortedArray.indexOf(criterionSortArray[i]) === -1) {
          sortedArray.push(criterionSortArray[i]);
        }
        sortedArray.push(array[k])
      }
    }
  }

  return sortedArray;
}

export const splitNumberByComma = number => {
  let transformedNumberToString = String(number);
  let intermediateArray = [];

  for (let i = transformedNumberToString.length - 1; i >= 0; i--) {
    if (intermediateArray.length % 3 === 0 && intermediateArray.length !== 0) {
      intermediateArray.push(transformedNumberToString[i] + ',');
    } else {
      intermediateArray.push(transformedNumberToString[i])
    }
  }

  intermediateArray.reverse();
  intermediateArray.push('.00');
  intermediateArray.unshift('$')
  return intermediateArray.join('');
}

export const fitGenres = array => {
  const genresArray = array.map(item => item.name);
  const genre = genresArray.join(', ');
  return genre;
}

export const fitCountries = array => {
  const countryArray = array.map(item => item.name);
  const country = countryArray.join(', ');
  return country;
}

export const fitRuntime = runtime => {
  const hours = Math.trunc(runtime / 60);
  const minutes = runtime - 60 * hours;
  const fittedRuntime = `${hours}h ${minutes}min`;
  return fittedRuntime;
}
